class Hello {
    public static void main(String[] args) {
        CliReader reader = new CliReader();
        String name;

        name = reader.input("What is your name, friend? ");
        System.out.println("Well hello, " + name + "! Welcome to my program.");
    }
}
