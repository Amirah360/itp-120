#1. What are the Three Laws of Test-driven Development?

Write No production code except to pass a failing test

Write only enough of a test to demostrate a failure

Write only enouhg production code to pass a test

#2. Explain the Red -> Green -> Refactor -> Red process.

create tests first in order to have clean code

#3. What are the three characteristics Uncle Bob lists of rotting code? (Note: these were covered in Episode 1).

Tangled warped and perverted 
Rigid fragile and immobile 
Debugging becomes complicated

#4. Explain how fear promotes code rot and why the fear exists in the first place. How does TDD help us break this vicious cycle?

 People don’t clean their code because they’re afraid because if you touch it you’ll break it 
So you  leave it in the system and it gradually rotscand then changes that should take days take weeks instead 
You can’t clean code unless you eliminate the fear of change 

#5. Uncle Bob mentions FitNesse as an example of a project that uses TDD effectively. What is FitNesse? What does it do?

 The fitnesse project is a Q and A process checks for bugs using TDD.

#6. What does Uncle Bob say about a program with a long bug list? What does he say this comes from?

Irresponsibility and carelessness and that the development team as been behaving unprofessionally

#7. What two other benefits does Uncle Bob say you get from TDD besides a reduction in debugging time?

Very high coverage and quick run times that it’s hard for a defect to get pass 
